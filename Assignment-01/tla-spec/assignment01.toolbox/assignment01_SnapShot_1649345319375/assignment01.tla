---------------------------- MODULE assignment01 ----------------------------
EXTENDS TLC, Integers, Sequences
CONSTANTS MaxQueueSize

(*--algorithm message_queue

variable taskPool = <<>>;
define
  TaskTemporalDependencies == ( 
    Len(taskPool) \in {0,1,2} 
  )
end define;

process Master \in { "Master" } 
variable task = "";
begin DistributeTasks:
  while TRUE do
    DistributeT1:
        await taskPool = <<>>;
        task := "task1";
        taskPool := Append(Append(taskPool, task), task);
    DistributeT2: 
        await taskPool = <<>>;
        task := "task2";
        taskPool := Append(Append(taskPool, task), task);
    DistributeT3:
        await taskPool = <<>>;
        task := "task3";
        taskPool := Append(Append(taskPool, task), task);
  end while;
end process;

process Worker \in { "Worker1", "Worker2" }
variable task = "none";
begin Execute:
  while TRUE do
    TakeTasks: 
        await taskPool /= <<>>;
        task := Head(taskPool);
        taskPool := Tail(taskPool);
    ExecuteTasks:
        print task;
  end while;
end process;
end algorithm;*)

\* BEGIN TRANSLATION (chksum(pcal) = "1849effc" /\ chksum(tla) = "d358bebf")
\* Process variable task of process Master at line 15 col 10 changed to task_
VARIABLES taskPool, pc

(* define statement *)
TaskTemporalDependencies == (
  Len(taskPool) \in {0,1,2}
)

VARIABLES task_, task

vars == << taskPool, pc, task_, task >>

ProcSet == ({ "Master" }) \cup ({ "Worker1", "Worker2" })

Init == (* Global variables *)
        /\ taskPool = <<>>
        (* Process Master *)
        /\ task_ = [self \in { "Master" } |-> ""]
        (* Process Worker *)
        /\ task = [self \in { "Worker1", "Worker2" } |-> "none"]
        /\ pc = [self \in ProcSet |-> CASE self \in { "Master" } -> "DistributeTasks"
                                        [] self \in { "Worker1", "Worker2" } -> "Execute"]

DistributeTasks(self) == /\ pc[self] = "DistributeTasks"
                         /\ pc' = [pc EXCEPT ![self] = "DistributeT1"]
                         /\ UNCHANGED << taskPool, task_, task >>

DistributeT1(self) == /\ pc[self] = "DistributeT1"
                      /\ taskPool = <<>>
                      /\ task_' = [task_ EXCEPT ![self] = "task1"]
                      /\ taskPool' = Append(Append(taskPool, task_'[self]), task_'[self])
                      /\ pc' = [pc EXCEPT ![self] = "DistributeT2"]
                      /\ task' = task

DistributeT2(self) == /\ pc[self] = "DistributeT2"
                      /\ taskPool = <<>>
                      /\ task_' = [task_ EXCEPT ![self] = "task2"]
                      /\ taskPool' = Append(Append(taskPool, task_'[self]), task_'[self])
                      /\ pc' = [pc EXCEPT ![self] = "DistributeT3"]
                      /\ task' = task

DistributeT3(self) == /\ pc[self] = "DistributeT3"
                      /\ taskPool = <<>>
                      /\ task_' = [task_ EXCEPT ![self] = "task3"]
                      /\ taskPool' = Append(Append(taskPool, task_'[self]), task_'[self])
                      /\ pc' = [pc EXCEPT ![self] = "DistributeTasks"]
                      /\ task' = task

Master(self) == DistributeTasks(self) \/ DistributeT1(self)
                   \/ DistributeT2(self) \/ DistributeT3(self)

Execute(self) == /\ pc[self] = "Execute"
                 /\ pc' = [pc EXCEPT ![self] = "TakeTasks"]
                 /\ UNCHANGED << taskPool, task_, task >>

TakeTasks(self) == /\ pc[self] = "TakeTasks"
                   /\ taskPool /= <<>>
                   /\ task' = [task EXCEPT ![self] = Head(taskPool)]
                   /\ taskPool' = Tail(taskPool)
                   /\ pc' = [pc EXCEPT ![self] = "ExecuteTasks"]
                   /\ task_' = task_

ExecuteTasks(self) == /\ pc[self] = "ExecuteTasks"
                      /\ PrintT(task[self])
                      /\ pc' = [pc EXCEPT ![self] = "Execute"]
                      /\ UNCHANGED << taskPool, task_, task >>

Worker(self) == Execute(self) \/ TakeTasks(self) \/ ExecuteTasks(self)

Next == (\E self \in { "Master" }: Master(self))
           \/ (\E self \in { "Worker1", "Worker2" }: Worker(self))

Spec == Init /\ [][Next]_vars

\* END TRANSLATION 

=============================================================================
\* Modification History
\* Last modified Thu Apr 07 17:28:32 CEST 2022 by Madi
\* Created Thu Apr 07 16:26:28 CEST 2022 by Madi
