package assignment01.junit.master;

import assignment01.concurrency.masterworker.master.SinglePoolMaster;

public class SinglePoolMasterTest extends MasterTest {
    protected SinglePoolMasterTest() {
        super(new SinglePoolMaster(Runtime.getRuntime().availableProcessors() + 1));
    }
}
