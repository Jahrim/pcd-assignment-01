package assignment01.junit.master;

import assignment01.concurrency.masterworker.master.MultiPoolMaster;

public class MultiPoolMasterTest extends MasterTest {
    protected MultiPoolMasterTest() {
        super(new MultiPoolMaster(Runtime.getRuntime().availableProcessors() + 1));
    }
}
