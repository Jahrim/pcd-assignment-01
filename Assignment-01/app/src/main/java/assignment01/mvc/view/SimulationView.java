package assignment01.mvc.view;

import java.util.ArrayList;
import assignment01.mvc.controller.Controller;
import assignment01.mvc.model.Body;
import assignment01.mvc.model.Boundary;

/**
 * Simulation view.
 */
public class SimulationView {
        
	private final VisualiserFrame frame;
	
    /**
     * Creates a view of the specified size (in pixels)
     * @param w the specified width
     * @param h the specified height
     */
    public SimulationView(int w, int h){
    	frame = new VisualiserFrame(w,h);
    }
        
    public void display(ArrayList<Body> bodies, double vt, long iter, Boundary bounds){
 	   frame.display(bodies, vt, iter, bounds); 
    }

    /**
     * Attach the specified controller to this viewer.
     * @param controller the specified controller
     */
    public void attachController(Controller controller){
        this.frame.attachController(controller);
    }

}
