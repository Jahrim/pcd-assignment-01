---------------------------- MODULE assignment01 ----------------------------
EXTENDS TLC, Integers, Sequences
CONSTANTS MaxQueueSize

(*--algorithm message_queue

variable 
    availableTaskPool = <<>>,
    completedTaskPool = Append(Append(<<>>, "task3"), "task3");

define
  \* It never happens that two tasks of different types are executed concurrently
  TaskExclusionInvariant == (Len(completedTaskPool) \in {0,1,2}) /\ (
    completedTaskPool = <<>> \/                                                                         \* completedTaskPool = []
    (Head(completedTaskPool) \in {"task1", "task2", "task3"} /\ Tail(completedTaskPool) = <<>>) \/      \* completedTaskPool = [taskX]
    Head(completedTaskPool) = Head(Tail(completedTaskPool))                                             \* completedTaskPool = [taskX,taskX]
  )
end define;

process Master \in { "Master" } 
variable task = "";
begin DistributeTasks:
  while TRUE do
    DistributeT1:
        await Len(completedTaskPool) = 2;
        completedTaskPool := <<>>;
        task := "task1";
        availableTaskPool := Append(Append(availableTaskPool, task), task);
    DistributeT2: 
        await Len(completedTaskPool) = 2;
        completedTaskPool := <<>>;
        task := "task2";
        availableTaskPool := Append(Append(availableTaskPool, task), task);
    DistributeT3:
        await Len(completedTaskPool) = 2;
        completedTaskPool := <<>>;
        task := "task3";
        availableTaskPool := Append(Append(availableTaskPool, task), task);
        print "end iteration"
  end while;
end process;

process Worker \in { "Worker1", "Worker2" }
variable task = "none";
begin Execute:
  while TRUE do
    TakeTasks: 
        await availableTaskPool /= <<>>;
        task := Head(availableTaskPool);
        availableTaskPool := Tail(availableTaskPool);
    ExecuteTasks:
        print task;
        completedTaskPool := Append(completedTaskPool, task);
  end while;
end process;
end algorithm;*)

\* BEGIN TRANSLATION (chksum(pcal) = "bf24d5c2" /\ chksum(tla) = "5ee6374b")
\* Process variable task of process Master at line 21 col 10 changed to task_
VARIABLES availableTaskPool, completedTaskPool, pc

(* define statement *)
TaskExclusionInvariant == (Len(completedTaskPool) \in {0,1,2}) /\ (
  completedTaskPool = <<>> \/
  (Head(completedTaskPool) \in {"task1", "task2", "task3"} /\ Tail(completedTaskPool) = <<>>) \/
  Head(completedTaskPool) = Head(Tail(completedTaskPool))
)

VARIABLES task_, task

vars == << availableTaskPool, completedTaskPool, pc, task_, task >>

ProcSet == ({ "Master" }) \cup ({ "Worker1", "Worker2" })

Init == (* Global variables *)
        /\ availableTaskPool = <<>>
        /\ completedTaskPool = Append(Append(<<>>, "task3"), "task3")
        (* Process Master *)
        /\ task_ = [self \in { "Master" } |-> ""]
        (* Process Worker *)
        /\ task = [self \in { "Worker1", "Worker2" } |-> "none"]
        /\ pc = [self \in ProcSet |-> CASE self \in { "Master" } -> "DistributeTasks"
                                        [] self \in { "Worker1", "Worker2" } -> "Execute"]

DistributeTasks(self) == /\ pc[self] = "DistributeTasks"
                         /\ pc' = [pc EXCEPT ![self] = "DistributeT1"]
                         /\ UNCHANGED << availableTaskPool, completedTaskPool, 
                                         task_, task >>

DistributeT1(self) == /\ pc[self] = "DistributeT1"
                      /\ Len(completedTaskPool) = 2
                      /\ completedTaskPool' = <<>>
                      /\ task_' = [task_ EXCEPT ![self] = "task1"]
                      /\ availableTaskPool' = Append(Append(availableTaskPool, task_'[self]), task_'[self])
                      /\ pc' = [pc EXCEPT ![self] = "DistributeT2"]
                      /\ task' = task

DistributeT2(self) == /\ pc[self] = "DistributeT2"
                      /\ Len(completedTaskPool) = 2
                      /\ completedTaskPool' = <<>>
                      /\ task_' = [task_ EXCEPT ![self] = "task2"]
                      /\ availableTaskPool' = Append(Append(availableTaskPool, task_'[self]), task_'[self])
                      /\ pc' = [pc EXCEPT ![self] = "DistributeT3"]
                      /\ task' = task

DistributeT3(self) == /\ pc[self] = "DistributeT3"
                      /\ Len(completedTaskPool) = 2
                      /\ completedTaskPool' = <<>>
                      /\ task_' = [task_ EXCEPT ![self] = "task3"]
                      /\ availableTaskPool' = Append(Append(availableTaskPool, task_'[self]), task_'[self])
                      /\ PrintT("end iteration")
                      /\ pc' = [pc EXCEPT ![self] = "DistributeTasks"]
                      /\ task' = task

Master(self) == DistributeTasks(self) \/ DistributeT1(self)
                   \/ DistributeT2(self) \/ DistributeT3(self)

Execute(self) == /\ pc[self] = "Execute"
                 /\ pc' = [pc EXCEPT ![self] = "TakeTasks"]
                 /\ UNCHANGED << availableTaskPool, completedTaskPool, task_, 
                                 task >>

TakeTasks(self) == /\ pc[self] = "TakeTasks"
                   /\ availableTaskPool /= <<>>
                   /\ task' = [task EXCEPT ![self] = Head(availableTaskPool)]
                   /\ availableTaskPool' = Tail(availableTaskPool)
                   /\ pc' = [pc EXCEPT ![self] = "ExecuteTasks"]
                   /\ UNCHANGED << completedTaskPool, task_ >>

ExecuteTasks(self) == /\ pc[self] = "ExecuteTasks"
                      /\ PrintT(task[self])
                      /\ completedTaskPool' = Append(completedTaskPool, task[self])
                      /\ pc' = [pc EXCEPT ![self] = "Execute"]
                      /\ UNCHANGED << availableTaskPool, task_, task >>

Worker(self) == Execute(self) \/ TakeTasks(self) \/ ExecuteTasks(self)

Next == (\E self \in { "Master" }: Master(self))
           \/ (\E self \in { "Worker1", "Worker2" }: Worker(self))

Spec == Init /\ [][Next]_vars

\* END TRANSLATION 

=============================================================================
\* Modification History
\* Last modified Thu Apr 07 18:02:24 CEST 2022 by Madi
\* Created Thu Apr 07 16:26:28 CEST 2022 by Madi
